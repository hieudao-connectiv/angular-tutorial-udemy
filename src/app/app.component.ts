import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = 'Hieu';
  role = 'Developer';
  company = 'Connectiv Viet Nam';
  img = 'https://picsum.photos/200/100';

  colspan = 2;
  pageHeader = 'Student Details';
  FirstName = 'Anurag';
  LastName = 'Mohanty';
  Branch = 'CSE';
  Mobile = 9876543210;
  Gender = 'Male';
  Age = 22;

  ClassesToApply = 'italicClass boldClass';
  ApplyBoldClass = true;
  ApplyItalicClass = true;
  AddCSSClasses() {
    let Cssclasses = {
      boldClass: this.ApplyBoldClass,
      italicClass: this.ApplyItalicClass,
    };
    return Cssclasses;
  }

  getName(): string {
    return this.name + ' - ' + this.role;
  }
}
