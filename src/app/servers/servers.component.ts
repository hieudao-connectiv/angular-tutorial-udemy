import { Component, OnInit } from '@angular/core';

@Component({

  // 'app-servers' actually works like a CSS selector. Therefore, you are not limited to selecting by element.
  selector: 'app-servers',

  templateUrl: './servers.component.html',
  // template: `
  // <app-server></app-server>

  // <app-server></app-server>
  // `,
  // styleUrls: ['./servers.component.css', '../app.component.css']
  styles: [`
    h3 {
      color: red;
    }
  `]

})
export class ServersComponent implements OnInit {

  allowNewServer = false;
  maliciousText = 'Hello <script>alert(\'your application is hacked\')</script>';
  serverCreationStatus = 'No server is created!';
  serverName = 'Test Server';

  allowNewChallengeServer = false;
  challengeServerStatus = 'No challenge server is created!'
  challengeServerName = '';
  challengeCreatedServer = false;

  servers = ['TestServer', 'TestServer 2'];

  paragraph = 'Any content you want type';
  clicks = [];
  showSecret = true;


  isValid = true;
  changeData(valid: boolean) {
    this.isValid = valid;
  }


  constructor() {
    setTimeout(() => {
      console.log('settimeout');
      this.allowNewServer = true;
    }, 2000);
  }

  ngOnInit(): void {

  }

  onUpdateServerName(event: Event) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }

  onCreateServer(): void {
    this.serverCreationStatus = 'Server was created! Name is ' + this.serverName;
  }

  onCreateChallengeServer() {
    this.challengeCreatedServer = true;
    this.servers.push(this.serverName)
    this.challengeServerStatus = 'challengeServerName was created! Name is ' + this.challengeServerName;
  }

  toggleDisplay() {
    this.clicks.push(this.clicks.length + 1);
    this.showSecret = !this.showSecret;
  }

}
